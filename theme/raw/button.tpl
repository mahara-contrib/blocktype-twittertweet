<div class="{$align}">
     <a href="https://twitter.com/share?url={$fullselfurl}"
        class="twitter-share-button"
        data-text="{$tweettext}"
        data-count="{$layout}"
        data-lang="{$localecode}">{str tag=tweet section=blocktype.twittertweet}</a>
<script type="text/javascript" src="https://platform.twitter.com/widgets.js"></script>
</div>
