<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2011 Joan Queralt i Gil jqueralt a gmail punt com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-twittertweet
 * @author     Joan Queralt i Gil
 */

defined('INTERNAL') || die();

$string['title'] = 'Tweet aTwitter ';
$string['description'] = 'Afegeix el botó Publica aquesta pàgina a Twitter';

$string['showtitle'] = 'Voleu mostrar el títol del bloc?';

$string['datacount'] = 'Estil del disseny';
$string['datacountdesc'] = 'Determineu la quantitat de vegades que s\'ha tuitejat des del botó.';
$string['datacountnone'] = 'No comptis';
$string['datacounthorizontal'] = 'Número en horitzontal';
$string['datacountvertical'] = 'Número en vertical';

$string['tweettext'] = 'Text del tweet';
$string['tweettextdesc'] = 'Aquest és el text que els usuaris inclouran al seu tweet quan facin clic al botó <b>Publica aquesta pàgina a Twitter</b> des d\'aquest lloc web.';

$string['align'] = 'Alineació';
$string['alignleft'] = 'Esquerra';
$string['aligncenter'] = 'Centre';
$string['alignright'] = 'Dreta';
