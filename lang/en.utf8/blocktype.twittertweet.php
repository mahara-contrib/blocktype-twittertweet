<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2011 Gregor Anzelj, gregor.anzelj@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-twittertweet
 * @author     Gregor Anzelj
 */

defined('INTERNAL') || die();

$string['title'] = 'Twitter Tweet';
$string['description'] = 'Add Twitter Tweet button';

$string['showtitle'] = 'Show Block Title?';

$string['datacount'] = 'Layout Style';
$string['datacountdesc'] = 'Determine the size and amount of social context next to the button.';
$string['datacountnone'] = 'No count';
$string['datacounthorizontal'] = 'Horizontal count';
$string['datacountvertical'] = 'Vertical count';

$string['tweet'] = 'Tweet';
$string['tweettext'] = 'Tweet text';
$string['tweettextdesc'] = 'This is the text that people will include in their Tweet when they share from your website.';

$string['align'] = 'Align';
$string['alignleft'] = 'Left';
$string['aligncenter'] = 'Center';
$string['alignright'] = 'Right';
