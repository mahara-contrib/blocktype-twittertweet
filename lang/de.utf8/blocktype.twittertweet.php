<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2011 Gregor Anzelj, gregor.anzelj@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-twittertweet
 * @author     Gregor Anzelj
 */

defined('INTERNAL') || die();

$string['title'] = 'Tweet-Schaltfläche';
$string['description'] = 'Fügt die Twitter Tweet-Schaltfläche hinzu.';

$string['showtitle'] = 'Blocktitel anzeigen?';

$string['datacount'] = 'Layout-Stil';
$string['datacountdesc'] = 'Legt die Anzeige der Tweet-Schaltfläche fest.';
$string['datacountnone'] = 'Ohne Zähler';
$string['datacounthorizontal'] = 'Horizontaler Zähler';
$string['datacountvertical'] = 'Vertikaler Zähler';

$string['tweet'] = 'Tweet';
$string['tweettext'] = 'Tweet-Text';
$string['tweettextdesc'] = 'Diesen Text erscheint als Standard-Text, wenn ein Tweet geschrieben wird.';

$string['align'] = 'Ausrichtung';
$string['alignleft'] = 'Links';
$string['aligncenter'] = 'Zentriert';
$string['alignright'] = 'Rechts';
