<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2011 Gregor Anzelj, gregor.anzelj@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-twittertweet
 * @author     Gregor Anzelj
 */

defined('INTERNAL') || die();

$string['title'] = 'Twitter Tweet';
$string['description'] = 'Dodajte gumb Twitter Tweet';

$string['showtitle'] = 'Prikažem naslov bloka?';

$string['datacount'] = 'Izgled gumba';
$string['datacountdesc'] = 'Določite velikost in količino družabne vsebine, prikazane poleg gumba.';
$string['datacountnone'] = 'Brez števca';
$string['datacounthorizontal'] = 'Gumb z vodoravnim števcem';
$string['datacountvertical'] = 'Gumb z navpičnim števcem';

$string['align'] = 'Poravnava';
$string['alignleft'] = 'Levo';
$string['aligncenter'] = 'Sredinsko';
$string['alignright'] = 'Desno';
