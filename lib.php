<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2011 Gregor Anzelj, gregor.anzelj@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-twittertweet
 * @author     Gregor Anzelj
 */

defined('INTERNAL') || die();

class PluginBlocktypeTwitterTweet extends SystemBlocktype {

    public static function single_only() {
        return true;
    }

    public static function get_title() {
        return get_string('title', 'blocktype.twittertweet');
    }

    public static function get_description() {
        return get_string('description', 'blocktype.twittertweet');
    }

    public static function get_categories() {
        return array('general');
    }

    public static function render_instance(BlockInstance $instance, $editing=false) {
        global $CFG;

        $configdata = $instance->get('configdata');
        $align      = !empty($configdata['align']) ? $configdata['align'] : 'left';
        $tweettext  = !empty($configdata['tweettext']) ? $configdata['tweettext'] : '';
        $tweetuser  = !empty($configdata['tweetuser']) ? $configdata['tweetuser'] : '';
        $layout     = !empty($configdata['layout']) ? $configdata['layout'] : 'none';

        $url = $CFG->wwwroot . 'view/view.php?id=' . $instance->get('id');

        $smarty = smarty_core();
        $smarty->assign('fullselfurl', urlencode($url));
        $smarty->assign('align', $align);
        $smarty->assign('tweettext', strip_tags($tweettext));
        $smarty->assign('layout', strip_tags($layout));
        $smarty->assign('localecode', self::get_locale_code(get_config('lang')));
        return $smarty->fetch('blocktype:twittertweet:button.tpl');
    }

    public static function has_instance_config() {
        return true;
    }

    public static function instance_config_form($instance) {
        $configdata = $instance->get('configdata');

        return array(
            'showtitle' => array(
                'type'  => 'checkbox',
                'title' => get_string('showtitle','blocktype.twittertweet'),
                'defaultvalue' => !empty($configdata['showtitle']) ? (bool)$configdata['showtitle'] : false,
            ),
            'layout' => array(
                'type'  => 'select',
                'title' => get_string('datacount','blocktype.twittertweet'),
                'description'  => get_string('datacountdesc','blocktype.twittertweet'),
                'defaultvalue' => !empty($configdata['layout']) ? $configdata['layout'] : 'none',
                'options' => array(
                    'none'       => get_string('datacountnone','blocktype.twittertweet'),
                    'horizontal' => get_string('datacounthorizontal','blocktype.twittertweet'),
                    'vertical'   => get_string('datacountvertical','blocktype.twittertweet'),
                ),
            ),
            'tweettext' => array(
                'type'  => 'text',
                'title' => get_string('tweettext','blocktype.twittertweet'),
                'description'  => get_string('tweettextdesc','blocktype.twittertweet'),
                'defaultvalue' => !empty($configdata['tweettext']) ? $configdata['tweettext'] : '',
            ),
            'align' => array(
                'type'  => 'radio',
                'title' => get_string('align','blocktype.twittertweet'),
                'defaultvalue' => !empty($configdata['align']) ? $configdata['align'] : 'left',
                'options' => array(
                    'left'   => get_string('alignleft','blocktype.twittertweet'),
                    'center' => get_string('aligncenter','blocktype.twittertweet'),
                    'right'  => get_string('alignright','blocktype.twittertweet'),
                ),
                'separator' => '&nbsp;&nbsp;&nbsp;',
            ),
        );
    }

    public static function instance_config_save($values) {
        if (empty($values['showtitle'])) {
			$values['title'] = null;
		}
		return $values;
	}

    public static function get_locale_code($code) {
		switch ($code) {
			case 'ca.utf8':
                return 'es';
			case 'de.utf8':
                return 'de';
			case 'es.utf8':
                return 'es';
			case 'fr.utf8':
                return 'fr';
			case 'it.utf8':
                return 'it';
			case 'jp.utf8':
                return 'jp';
			case 'ko.utf8':
                return 'ko';
			default:
                return 'en';
		}
	}

    public static function default_copy_type() {
        return 'full';
    }
}
